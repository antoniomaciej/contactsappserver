namespace ContactsApp.Server.Infrastructure.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using ContactsApp.Server.Domain.Contacts;
    using ContactsApp.Server.Infrastructure.Mapping;
    using ContactsApp.Server.Infrastructure.Repositories;
    using ContactsApp.Server.Infrastructure.Services.Contacts;
    using FluentAssertions;
    using Moq;
    using Xunit;
    using ContactsDto = ContactsApp.Server.Infrastructure.Dto.Contacts;

    public class ContactServiceTest
    {
        private readonly MapperConfiguration configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ContactsProfile>();
        });

        [Fact]
        public void GetLatestContacts_ShouldReturnEmptyCollectionWhenNoContacts()
        {
            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(x => x.GetContacts()).Returns(new List<Contact>());
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            sut.GetRecentlyAddedContacts().Should().BeEmpty();
        }

        [Fact]
        public void GetLatestContacts_ShouldReturnAtMost10Contacts()
        {
            var dbContacts = Enumerable
                .Range(1, 12)
                .Select(n => new Contact { Id = n, CreatedWhen = DateTime.UnixEpoch.AddDays(-n) });

            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(x => x.GetContacts()).Returns(dbContacts);
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            var actual = sut.GetRecentlyAddedContacts();

            actual.Should().HaveCount(10);
            actual
                .Where(c => c.CreatedWhen < DateTime.UnixEpoch.AddDays(-10))
                .Should()
                .BeEmpty();
        }

        [Fact]
        public void GetLatestContacts_ShouldReturnAllContactsIfLessThan10()
        {
            var dbContacts = Enumerable
                .Range(1, 5)
                .Select(n => new Contact { Id = n, CreatedWhen = DateTime.UnixEpoch.AddDays(-n) });

            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(x => x.GetContacts()).Returns(dbContacts);
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            var actual = sut.GetRecentlyAddedContacts();

            actual.Should().HaveCount(5);
        }

        [Fact]
        public void GetLatestContacts_ShouldReturnEmptyCollectionIfNull()
        {
            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(x => x.GetContacts()).Returns(null as IEnumerable<Contact>);
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            sut.GetRecentlyAddedContacts().Should().BeEmpty();
        }

        [Fact]
        public async Task AddNewContact_ShouldNotAllowToAddAContactWithMoreThan3Emails()
        {
            var mockRepository = new Mock<IContactRepository>();
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            var result = await sut.AddNewContact(
                new ContactsDto.Contact
                {
                    Emails = new List<string> { "a", "b", "c", "d" },
                });
            mockRepository.Verify(repo => repo.SaveContact(It.IsAny<Contact>()), Times.Never);
            result.IsLeft.Should().BeTrue();
            result
                .Match<string>(
                    Left: res => res,
                    Right: n => "fail")
                .Should()
                .Be("A contact may have at most 3 emails.");
        }

        [Fact]
        public async Task AddNewContact_ShouldNotAllowToAddAContactWithMoreThan3PhoneNumbers()
        {
            var mockRepository = new Mock<IContactRepository>();
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            var result = await sut.AddNewContact(
                new ContactsDto.Contact
                {
                    PhoneNumbers = new List<string> { "a", "b", "c", "d" },
                });
            mockRepository.Verify(repo => repo.SaveContact(It.IsAny<Contact>()), Times.Never);
            result.IsLeft.Should().BeTrue();
            result
                .Match<string>(
                    Left: res => res,
                    Right: n => "fail")
                .Should()
                .Be("A contact may have at most 3 phone numbers.");
        }

        [Fact]
        public async Task AddNewContact_ShouldCallRepository()
        {
            var mockRepository = new Mock<IContactRepository>();
            var sut = new ContactService(mockRepository.Object, configuration.CreateMapper());
            ContactsDto.Contact contact = new ContactsDto.Contact
            {
                FirstName = "Juan",
                LastName = "Perez",
                CompanyName = "Acme Inc.",
                PhoneNumbers = new List<string> { "a", "b", "c" },
                Emails = new List<string> { "a", "b", "c" },
            };
            mockRepository
                .Setup(x =>
                    x.SaveContact(It.IsAny<Contact>()))
                .Returns(Task.FromResult<Contact>(new Contact { Id = 1 }));
            var result = await sut.AddNewContact(contact);
            mockRepository.Verify(repo => repo.SaveContact(It.IsAny<Contact>()), Times.Once);
            result.IsRight.Should().BeTrue();
            result.Match<long>(
                    Left: s => 1000L,
                    Right: c => c.Id.Value)
                .Should()
                .Be(1);
        }
    }
}
