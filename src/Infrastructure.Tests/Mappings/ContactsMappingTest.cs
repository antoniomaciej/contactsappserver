namespace ContactsApp.Server.Infrastructure.Tests.Mappings
{
    using AutoMapper;
    using ContactsApp.Server.Infrastructure.Mapping;
    using Xunit;

    public class ContactsMappingTest
    {
        [Fact]
        public void ContactsMapping_ShouldBeValid()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ContactsProfile>();
            });

            configuration.AssertConfigurationIsValid();
        }
    }
}
