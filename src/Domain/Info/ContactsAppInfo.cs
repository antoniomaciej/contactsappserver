namespace ContactsApp.Server.Domain.Info
{
    using System;

    public static class ContactsAppInfo
    {
        public const string Title = "Contacts App";

        public const string Version = "V1";

        public const string Description = "Store, search and edit your company contacts.";

        public const string XAudience = "company-internal";

        public static readonly Guid XApiId = new Guid("c25ecd37-61b5-4d80-9c07-0f788f7fb31f");
    }
}
