namespace ContactsApp.Server.Domain.Info
{
    public class MaintainerInfo
    {
        public const string MaintainerName = "Antonio Maciej Matamoros Ochman";

        public const string MaintainerEmail = "antoniomaciejmo@gmail.com";

        public string Name { get; } = MaintainerName;

        public string Email { get; } = MaintainerEmail;
    }
}
