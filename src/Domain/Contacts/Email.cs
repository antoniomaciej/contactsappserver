namespace ContactsApp.Server.Domain.Contacts
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using ContactsApp.Server.Domain.Auditability;

    public class Email : AuditableEntity
    {
        public long ContactId { get; set; }

        [Required]
        [EmailAddress]
        [Column("email")]
        [StringLength(50)]
        public string ContactEmail { get; set; }
    }
}
