namespace ContactsApp.Server.Domain.Contacts
{
    using System.ComponentModel.DataAnnotations;
    using ContactsApp.Server.Domain.Auditability;

    public class PhoneNumber : AuditableEntity
    {
        public long ContactId { get; set; }

        [Required]
        [Phone]
        [StringLength(50)]
        public string Number { get; set; }
    }
}
