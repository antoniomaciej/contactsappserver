namespace ContactsApp.Server.Domain.Contacts
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using ContactsApp.Server.Domain.Auditability;

    public class Contact : AuditableEntity
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        [Required]
        [StringLength(255)]
        public string CompanyName { get; set; }

        [StringLength(100)]
        public string Position { get; set; }

        public virtual IEnumerable<Email> Emails { get; set; }

        public virtual IEnumerable<PhoneNumber> PhoneNumbers { get; set; }
    }
}
