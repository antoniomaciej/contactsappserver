namespace ContactsApp.Server.Domain.Auditability
{
    using System;

    public class AuditableEntity
    {
        public long Id { get; set; }

        public DateTime CreatedWhen { get; set; }

        public DateTime? DeletedWhen { get; set; }

        public DateTime ModifiedWhen { get; set; }
    }
}
