namespace ContactsApp.Server.Start
{
    using AutoMapper;
    using ContactsApp.Server.Domain.Info;
    using ContactsApp.Server.Infrastructure.DataAccess;
    using ContactsApp.Server.Infrastructure.Mapping;
    using ContactsApp.Server.Infrastructure.Repositories;
    using ContactsApp.Server.Infrastructure.Services.Contacts;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;

    public class Startup
    {
        private const string SwaggerVersion = "v1";
        private const string SwaggerEndpoint = "/swagger/v1/swagger.json";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(
                    SwaggerVersion,
                    new OpenApiInfo { Title = ContactsAppInfo.Title, Version = ContactsAppInfo.Version });
            });
            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services
                .AddDbContext<ContactContext>(options =>
                    options
                        .UseNpgsql(Configuration.GetConnectionString("ContactsAppDatabase"))
                        .UseSnakeCaseNamingConvention());
            services.AddAutoMapper(typeof(ContactsProfile));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerEndpoint, $"{ContactsAppInfo.Title} {ContactsAppInfo.Version}");
            });
        }
    }
}
