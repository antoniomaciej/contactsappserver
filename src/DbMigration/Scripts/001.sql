create table if not exists contacts (id serial primary key,
first_name varchar(50),
last_name varchar(50),
"comment" varchar(255),
company_name varchar(255),
created_when timestamp not null,
modified_when timestamp,
deleted_when timestamp);

create table if not exists emails (id serial primary key,
contact_id integer references contacts(id),
email varchar(50) not null,
created_when timestamp not null,
modified_when timestamp,
deleted_when timestamp);

create table if not exists phone_numbers (id serial primary key,
contact_id integer references contacts(id),
number varchar(50) not null,
created_when timestamp not null,
modified_when timestamp,
deleted_when timestamp);