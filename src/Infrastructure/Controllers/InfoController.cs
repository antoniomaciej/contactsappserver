﻿namespace ContactsApp.Server.Infrastructure.Controllers
{
    using System;
    using ContactsApp.Server.Domain.Info;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class InfoController : ControllerBase
    {
        public InfoController()
        {
        }

        [HttpGet]
        [Route("Title")]
        public string GetTitle()
        {
            return ContactsAppInfo.Title;
        }

        [HttpGet]
        [Route("Version")]
        public string GetVersion()
        {
            return ContactsAppInfo.Version;
        }

        [HttpGet]
        [Route("Description")]
        public string GetDescription()
        {
            return ContactsAppInfo.Description;
        }

        [HttpGet]
        [Route("Contact")]
        public MaintainerInfo GetContact()
        {
            return new MaintainerInfo();
        }

        [HttpGet]
        [Route("x-api-id")]
        public Guid GetXApiId()
        {
            return ContactsAppInfo.XApiId;
        }

        [HttpGet]
        [Route("x-audience")]
        public string GetXAudience()
        {
            return ContactsAppInfo.XAudience;
        }
    }
}
