﻿namespace ContactsApp.Server.Infrastructure.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController : ControllerBase
    {
        public HealthCheckController()
        {
        }

        [HttpGet]
        public void Get()
        {
            return;
        }
    }
}
