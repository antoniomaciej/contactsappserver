﻿namespace ContactsApp.Server.Infrastructure.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ContactsApp.Server.Infrastructure.Dto.Contacts;
    using ContactsApp.Server.Infrastructure.Services.Contacts;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class ContactsController : ControllerBase
    {
        private readonly IContactService contactService;

        public ContactsController(IContactService contactService)
        {
            this.contactService = contactService;
        }

        [HttpGet]
        [Route("RecentlyAddedContacts")]
        public IEnumerable<Contact> GetRecentlyAddedContactsContacts()
        {
            return this.contactService.GetRecentlyAddedContacts();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Contact>> GetContact(long id)
        {
            var result = (await this.contactService.GetById(id))
                .Match(
                    Some: c => new ActionResult<Contact>(Ok(c)),
                    None: () => new ActionResult<Contact>(NotFound()));

            return result;
        }

        [HttpPost]
        [Route("Create")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Contact>> PostContact(Contact contact)
        {
            var result = await this.contactService.AddNewContact(contact);

            return result.Match<ActionResult<Contact>>(
                Right: task =>
                {
                    var contactInTask = task;
                    return new ActionResult<Contact>(
                        CreatedAtAction(nameof(GetContact), new { id = contactInTask.Id }, contactInTask));
                },
                Left: message => new ActionResult<Contact>(BadRequest(message)));
        }
    }
}
