﻿namespace ContactsApp.Server.Infrastructure.Dto.Entities
{
    using System;

    public class AuditableEntityDto
    {
        public long? Id { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public DateTime? DeletedWhen { get; set; }

        public DateTime? ModifiedWhen { get; set; }
    }
}
