namespace ContactsApp.Server.Infrastructure.Dto.Contacts
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using ContactsApp.Server.Infrastructure.Dto.Entities;

    public class Contact : AuditableEntityDto
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        [Required]
        [StringLength(255)]
        public string CompanyName { get; set; }

        [StringLength(100)]
        public string Position { get; set; }

        public IEnumerable<string> Emails { get; set; }

        public IEnumerable<string> PhoneNumbers { get; set; }
    }
}
