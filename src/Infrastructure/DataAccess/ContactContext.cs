﻿namespace ContactsApp.Server.Infrastructure.DataAccess
{
    using System.Diagnostics.CodeAnalysis;
    using ContactsApp.Server.Domain.Contacts;
    using Microsoft.EntityFrameworkCore;

    public class ContactContext : DbContext
    {
        public ContactContext([NotNullAttribute] DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<Email> Emails { get; set; }

        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
    }
}
