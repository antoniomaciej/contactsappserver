namespace ContactsApp.Server.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ContactsApp.Server.Domain.Contacts;
    using ContactsApp.Server.Infrastructure.DataAccess;
    using LanguageExt;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Query;

    public class ContactRepository : IContactRepository
    {
        private readonly ContactContext contactsContext;

        public ContactRepository(ContactContext contactsContext)
        {
            this.contactsContext = contactsContext;
        }

        public async Task<Option<Contact>> GetById(long id)
        {
            var contactOrNull = (await GetAllContacts()
                .ToListAsync())
                .Where(c => !c.DeletedWhen.HasValue)
                .SingleOrDefault(e => e.Id == id);

            return contactOrNull;
        }

        public IEnumerable<Contact> GetContacts()
        {
            var contacts = GetAllContacts()
               .ToList()
               .Where(c => !c.DeletedWhen.HasValue);

            return contacts;
        }

        public async Task<Contact> SaveContact(Contact contact)
        {
            var now = DateTime.Now;
            contact.CreatedWhen = now;
            contact.ModifiedWhen = now;
            contact.DeletedWhen = null;
            this.contactsContext.Contacts.Add(contact);
            await this.contactsContext.SaveChangesAsync();

            return contact;
        }

        private IIncludableQueryable<Contact, IEnumerable<PhoneNumber>> GetAllContacts()
        {
            return contactsContext
                           .Contacts
                           .Include(c => c.Emails)
                           .Include(c => c.PhoneNumbers);
        }
    }
}
