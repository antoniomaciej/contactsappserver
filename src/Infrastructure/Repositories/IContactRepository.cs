namespace ContactsApp.Server.Infrastructure.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ContactsApp.Server.Domain.Contacts;
    using LanguageExt;

    public interface IContactRepository
    {
        IEnumerable<Contact> GetContacts();

        Task<Contact> SaveContact(Contact contact);

        Task<Option<Contact>> GetById(long id);
    }
}
