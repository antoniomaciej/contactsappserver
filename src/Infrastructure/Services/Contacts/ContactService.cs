namespace ContactsApp.Server.Infrastructure.Services.Contacts
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using ContactsApp.Server.Domain.Contacts;
    using ContactsApp.Server.Infrastructure.Repositories;
    using LanguageExt;
    using ContactsDto = ContactsApp.Server.Infrastructure.Dto.Contacts;

    public class ContactService : IContactService
    {
        private const int RecentlyAddedCardinalityLimit = 10;

        private const string EmailCardinalityExceeded = "A contact may have at most 3 emails.";

        private const string PhoneNumbersCardinalityExceeded = "A contact may have at most 3 phone numbers.";

        private const int EmailMaxCardinality = 3;

        private const int PhoneNumberMaxCardinality = 3;

        private readonly IMapper mapper;

        private readonly IContactRepository contactRepository;

        public ContactService(IContactRepository contactRepo, IMapper mapper)
        {
            this.contactRepository = contactRepo;
            this.mapper = mapper;
        }

        public async Task<Either<string, ContactsDto.Contact>> AddNewContact(ContactsDto.Contact contact)
        {
            if (contact.Emails?.Count() > EmailMaxCardinality)
            {
                return EmailCardinalityExceeded;
            }

            if (contact.PhoneNumbers?.Count() > PhoneNumberMaxCardinality)
            {
                return PhoneNumbersCardinalityExceeded;
            }

            var result = await this
                .contactRepository
                .SaveContact(this.mapper.Map<ContactsDto.Contact, Contact>(contact));
            var mappedResult = mapper.Map<Contact, ContactsDto.Contact>(result);

            return mappedResult;
        }

        public async Task<Option<ContactsDto.Contact>> GetById(long id)
        {
            var mappedResult = (await this.contactRepository.GetById(id))
                .BiBind<ContactsDto.Contact>(
                    c => mapper.Map<Contact, ContactsDto.Contact>(c),
                    () => null);

            return mappedResult;
        }

        public IEnumerable<ContactsDto.Contact> GetRecentlyAddedContacts()
        {
            var contacts = contactRepository.GetContacts();
            var recentlyAdded = contacts?
                .OrderByDescending(contact => contact.CreatedWhen)
                .Take(RecentlyAddedCardinalityLimit) ??
                new List<Contact>();
            var recentlyAddedAsDto = mapper.Map<IEnumerable<Contact>, IEnumerable<ContactsDto.Contact>>(recentlyAdded);

            return recentlyAddedAsDto;
        }
    }
}
