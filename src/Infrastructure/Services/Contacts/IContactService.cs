namespace ContactsApp.Server.Infrastructure.Services.Contacts
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ContactsApp.Server.Infrastructure.Dto.Contacts;
    using LanguageExt;

    public interface IContactService
    {
        IEnumerable<Contact> GetRecentlyAddedContacts();

        Task<Either<string, Contact>> AddNewContact(Contact contact);

        Task<Option<Contact>> GetById(long id);
    }
}
