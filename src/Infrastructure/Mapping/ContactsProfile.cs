namespace ContactsApp.Server.Infrastructure.Mapping
{
    using System.Linq;
    using AutoMapper;
    using ContactsApp.Server.Domain.Contacts;
    using ContactsDto = ContactsApp.Server.Infrastructure.Dto.Contacts;

    public class ContactsProfile : Profile
    {
        public ContactsProfile()
        {
            CreateMap<Contact, ContactsDto.Contact>()
               .ForMember(
                   dest => dest.Emails,
                   opts => opts.MapFrom(src =>
                       src.Emails.Select(email => email.ContactEmail)))
               .ForMember(
                   dest => dest.PhoneNumbers,
                   opts => opts.MapFrom(src =>
                       src.PhoneNumbers.Select(pNumber => pNumber.Number)))
               .ForMember(
                   dest => dest.Id,
                   opts => opts.MapFrom(src => src.Id));
            CreateMap<ContactsDto.Contact, Contact>()
               .ForMember(
                   dest => dest.Emails,
                   opts => opts.MapFrom(src =>
                       src.Emails.Select(email => new Email { ContactEmail = email })))
               .ForMember(
                   dest => dest.PhoneNumbers,
                   opts => opts.MapFrom(src =>
                       src.PhoneNumbers.Select(pNumber => new PhoneNumber { Number = pNumber })));
        }
    }
}
